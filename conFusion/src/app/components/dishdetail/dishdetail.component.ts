import { Component, OnInit, ViewChild } from '@angular/core';
import { Dish } from 'src/app/shared/dish';
import { DishService } from 'src/app/services/dish.service';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { switchMap } from 'rxjs/operators';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-dishdetail',
  templateUrl: './dishdetail.component.html',
  styleUrls: ['./dishdetail.component.scss']
})
export class DishdetailComponent implements OnInit {
  commentFormErrors = {
    'author': '',
    'comment': ''
  };

  validationMessages = {
    'author': {
      'required': 'Name is required.',
      'minlength': 'Name must be at least 2 characters long.',
      'maxlength': 'Name cannot be more than 25 characters long.'
    },
    'comment': {
      'required': 'Comment is required.',
      'minlength': 'Comment must be at least 2 characters long.'
    }
  };
  @ViewChild('comntForm') commentFormDirective;
  dishIds: string[];
  prev: string;
  next: string;
  dish: Dish;
  commentForm: FormGroup;
  constructor(private dishService: DishService, private route: ActivatedRoute,
              private location: Location, private fb: FormBuilder) {
    this.commentForm = this.fb.group({
      author: ['',[Validators.required, Validators.minLength(2), Validators.maxLength(25)]],
      rating: 5,
      comment: ['',[Validators.required, Validators.minLength(2)]]      
    });
  }

  ngOnInit() {
    this.dishService.getDishIds().subscribe(ids=>this.dishIds = ids)    
    this.route.params.pipe(switchMap((params: Params)=>this.dishService.getDish(params['id'])))
    .subscribe(d=>{this.dish = d; this.setPrevNext(d.id);});
    this.commentForm.valueChanges
    .subscribe(data => this.onValueChanged(data));
    this.onValueChanged(); 
  }
  onValueChanged(data?: any): void {
    if (!this.commentForm) { return; }
    const form = this.commentForm;
    for (const field in this.commentFormErrors) {
      if (this.commentFormErrors.hasOwnProperty(field)) {
        // clear previous error message (if any)
        this.commentFormErrors[field] = '';
        const control = form.get(field);
        if (control && control.dirty && !control.valid) {
          const messages = this.validationMessages[field];
          for (const key in control.errors) {
            if (control.errors.hasOwnProperty(key)) {
              this.commentFormErrors[field] += messages[key] + ' ';
            }
          }
        }
      }
    }
  }
  setPrevNext(id: string) {
    const index = this.dishIds.indexOf(id);    
    this.prev = this.dishIds[(this.dishIds.length+index-1)%this.dishIds.length];
    this.next = this.dishIds[(this.dishIds.length+index+1)%this.dishIds.length];
  }
  goBack(){
    this.location.back();
  }
  onSubmit(authorName: string, comment: string, rating: number){
    const newComment = {
      rating: rating,
      comment: comment,
      author: authorName,
      date: new Date().toString()
    } 
    this.dish.comments.push(newComment);
    this.resetFormData();
    this.commentFormDirective.resetForm();
  }
  resetFormData(){
    this.commentForm.reset({
      author: '',      
      comment: '',
      rating: 5     
    });
  }
}
