import { Component, OnInit } from '@angular/core';
import { Dish } from 'src/app/shared/dish';
import { Promotion } from 'src/app/shared/promotion';
import { DishService } from 'src/app/services/dish.service';
import { PromotionService } from 'src/app/services/promotion.service';
import { Leader } from 'src/app/shared/leader';
import { LeaderService } from 'src/app/services/leader.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  dish: Dish;
  promotion: Promotion;
  leader: Leader;
  constructor(private dishService: DishService, private promotionService: PromotionService,
    private leaderService: LeaderService) {

   }

  ngOnInit() {
    this.dishService.getFeaturedDish()
    .subscribe(d => this.dish = d);
    this.promotionService.getFeaturedPromotion()
    .subscribe(promotion => this.promotion=promotion);
    this.leaderService.getFeaturedLeader()
    .subscribe(leader =>this.leader=leader);
  }

}
