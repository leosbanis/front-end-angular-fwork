import { Promotion } from './promotion';

export const PROMOTIONS: Promotion[] = [
    {
        id:'0',
        name: 'Weekend Grand Buffet',
        image: '/assets/images/buffet.png',
        label: 'New',
        price: '19.99',
        featured: true,
        //tslint: disable-next-line:max-line-length,
        description: `Featuring Mouthwatering combinations with a choise of five diferents salads,
             six enticing appetizers, six main entrees and five choiset desserts. 
             Free flowing bubbly and soft drinks. All for just $19.99 per person`
    }
];